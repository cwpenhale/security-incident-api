= Security Incident API
:toc:

== Building and Running The Project

This project can be built and run by using the `mvn spring-boot:run` command. If you do not have an existing Java and Maven installation, I recommend using https://sdkman.io/[SDKMan] to install maven and java.

To install maven and java with SDKMan:
[source,bash]
----
~/git/security-incident-api$ sdk install maven

Found a previously downloaded maven 3.8.1 archive. Not downloading it again...

Installing: maven 3.8.1
Done installing!


Setting maven 3.8.1 as default.
~/git/security-incident-api$ sdk install java 16.0.1.hs-adpt

Found a previously downloaded java 16.0.1.hs-adpt archive. Not downloading it again...

Installing: java 16.0.1.hs-adpt
Done installing!

Do you want java 16.0.1.hs-adpt to be set as default? (Y/n): y

Setting java 16.0.1.hs-adpt as default.
----

The expected output of the maven task to run SpringBoot is as follows:
----
cpenhale@DESKTOP-PBVM14P:~/git/security-incident-api$ mvn spring-boot:run
[INFO] Scanning for projects...
[INFO]
[INFO] -----------< com.connorpenhale.demos:security-incident-api >------------
[INFO] Building A Camel Spring Boot Route 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] >>> spring-boot-maven-plugin:2.4.5:run (default-cli) > test-compile @ security-incident-api >>>
[INFO]
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ security-incident-api ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 3 resources
[INFO]
[INFO] --- maven-compiler-plugin:3.8.1:compile (default-compile) @ security-incident-api ---
[INFO] Nothing to compile - all classes are up to date
[INFO]
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ security-incident-api ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 0 resource
[INFO]
[INFO] --- maven-compiler-plugin:3.8.1:testCompile (default-testCompile) @ security-incident-api ---
[INFO] Nothing to compile - all classes are up to date
[INFO]
[INFO] <<< spring-boot-maven-plugin:2.4.5:run (default-cli) < test-compile @ security-incident-api <<<
[INFO]
[INFO]
[INFO] --- spring-boot-maven-plugin:2.4.5:run (default-cli) @ security-incident-api ---
[INFO] Attaching agents: []

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v2.4.5)

2021-07-15 21:18:24.891  INFO 16767 --- [           main] c.c.demos.MySpringBootApplication        : Starting MySpringBootApplication using Java 16.0.1 on DESKTOP-PBVM14P with PID 16767 (/home/cpenhale/git/security-incident-api/target/classes started by cpenhale in /home/cpenhale/git/security-incident-api)
2021-07-15 21:18:24.892  INFO 16767 --- [           main] c.c.demos.MySpringBootApplication        : No active profile set, falling back to default profiles: default
2021-07-15 21:18:25.840  WARN 16767 --- [           main] io.undertow.websockets.jsr               : UT026010: Buffer pool was not set on WebSocketDeploymentInfo, the default pool will be used
2021-07-15 21:18:25.854  INFO 16767 --- [           main] io.undertow.servlet                      : Initializing Spring embedded WebApplicationContext
2021-07-15 21:18:25.854  INFO 16767 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 925 ms
2021-07-15 21:18:26.100  INFO 16767 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
2021-07-15 21:18:26.273  WARN 16767 --- [           main] o.a.c.i.e.DefaultCamelBeanPostProcessor  : No CamelContext defined yet so cannot inject into bean: org.apache.camel.converter.stream.StreamCacheBulkConverterLoader
2021-07-15 21:18:26.274  WARN 16767 --- [           main] o.a.c.i.e.DefaultCamelBeanPostProcessor  : No CamelContext defined yet so cannot inject into bean: org.apache.camel.component.file.GenericFileConverterLoader
2021-07-15 21:18:26.276  WARN 16767 --- [           main] o.a.c.i.e.DefaultCamelBeanPostProcessor  : No CamelContext defined yet so cannot inject into bean: org.apache.camel.converter.CamelBaseBulkConverterLoader
2021-07-15 21:18:26.277  WARN 16767 --- [           main] o.a.c.i.e.DefaultCamelBeanPostProcessor  : No CamelContext defined yet so cannot inject into bean: org.apache.camel.component.jackson.converter.JacksonTypeConvertersLoader
2021-07-15 21:18:26.277  WARN 16767 --- [           main] o.a.c.i.e.DefaultCamelBeanPostProcessor  : No CamelContext defined yet so cannot inject into bean: org.apache.camel.component.netty.http.NettyHttpConverterLoader
2021-07-15 21:18:26.280  WARN 16767 --- [           main] o.a.c.i.e.DefaultCamelBeanPostProcessor  : No CamelContext defined yet so cannot inject into bean: org.apache.camel.component.netty.NettyConverterLoader
2021-07-15 21:18:26.282  WARN 16767 --- [           main] o.a.c.i.e.DefaultCamelBeanPostProcessor  : No CamelContext defined yet so cannot inject into bean: org.apache.camel.component.http.HttpEntityConverterLoader
2021-07-15 21:18:26.283  WARN 16767 --- [           main] o.a.c.i.e.DefaultCamelBeanPostProcessor  : No CamelContext defined yet so cannot inject into bean: org.apache.camel.http.common.HttpConverterLoader
2021-07-15 21:18:26.285  WARN 16767 --- [           main] o.a.c.i.e.DefaultCamelBeanPostProcessor  : No CamelContext defined yet so cannot inject into bean: org.apache.camel.attachment.AttachmentConverterLoader
2021-07-15 21:18:26.286  WARN 16767 --- [           main] o.a.c.i.e.DefaultCamelBeanPostProcessor  : No CamelContext defined yet so cannot inject into bean: org.apache.camel.spring.converter.ResourceConverterLoader
2021-07-15 21:18:26.289  WARN 16767 --- [           main] o.a.c.i.e.DefaultCamelBeanPostProcessor  : No CamelContext defined yet so cannot inject into bean: org.apache.camel.converter.jaxp.CamelXmlJaxpBulkConverterLoader
2021-07-15 21:18:26.511  INFO 16767 --- [           main] o.s.b.a.e.web.EndpointLinksResolver      : Exposing 2 endpoint(s) beneath base path '/actuator'
2021-07-15 21:18:26.558  INFO 16767 --- [           main] io.undertow                              : starting server: Undertow - 2.2.7.Final
2021-07-15 21:18:26.565  INFO 16767 --- [           main] org.xnio                                 : XNIO version 3.8.0.Final
2021-07-15 21:18:26.571  INFO 16767 --- [           main] org.xnio.nio                             : XNIO NIO Implementation Version 3.8.0.Final
2021-07-15 21:18:26.617  INFO 16767 --- [           main] org.jboss.threads                        : JBoss Threads version 3.1.0.Final
2021-07-15 21:18:26.665  INFO 16767 --- [           main] o.s.b.w.e.undertow.UndertowWebServer     : Undertow started on port(s) 8080 (http)
2021-07-15 21:18:26.965  INFO 16767 --- [           main] o.a.camel.component.http.HttpComponent   : Created ClientConnectionManager org.apache.http.impl.conn.PoolingHttpClientConnectionManager@136fece2
2021-07-15 21:18:27.069  INFO 16767 --- [           main] o.a.c.c.n.h.HttpServerBootstrapFactory   : BootstrapFactory on port 9000 is using bootstrap configuration: [NettyServerBootstrapConfiguration{protocol='http', host='localhost', port=9000, broadcast=false, sendBufferSize=65536, receiveBufferSize=65536, receiveBufferSizePredictor=0, workerCount=0, bossCount=1, keepAlive=true, tcpNoDelay=true, reuseAddress=true, connectTimeout=10000, backlog=0, serverInitializerFactory=org.apache.camel.component.netty.http.HttpServerInitializerFactory@369a95a5, nettyServerBootstrapFactory=null, options=null, ssl=false, sslHandler=null, sslContextParameters='null', needClientAuth=false, enabledProtocols='TLSv1,TLSv1.1,TLSv1.2, keyStoreFile=null, trustStoreFile=null, keyStoreResource='null', trustStoreResource='null', keyStoreFormat='JKS', securityProvider='SunX509', passphrase='null', bossGroup=null, workerGroup=null, networkInterface='null', reconnect='true', reconnectInterval='10000'}]
2021-07-15 21:18:27.078  INFO 16767 --- [           main] o.a.c.component.netty.NettyComponent     : Creating shared NettyConsumerExecutorGroup with 65 threads
2021-07-15 21:18:27.108  INFO 16767 --- [           main] o.a.c.c.j.AbstractJacksonDataFormat      : The option autoDiscoverObjectMapper is set to false, Camel won't search in the registry
2021-07-15 21:18:27.109  INFO 16767 --- [           main] o.a.c.c.j.AbstractJacksonDataFormat      : The option autoDiscoverObjectMapper is set to false, Camel won't search in the registry
2021-07-15 21:18:27.109  INFO 16767 --- [           main] o.a.c.c.j.AbstractJacksonDataFormat      : The option autoDiscoverObjectMapper is set to false, Camel won't search in the registry
2021-07-15 21:18:27.131  INFO 16767 --- [           main] c.n.SingleTCPNettyServerBootstrapFactory : ServerBootstrap binding to localhost:9000
2021-07-15 21:18:27.169  INFO 16767 --- [           main] o.a.camel.component.netty.NettyConsumer  : Netty consumer bound to: localhost:9000
2021-07-15 21:18:27.172  INFO 16767 --- [           main] o.a.c.impl.engine.AbstractCamelContext   : Routes startup summary (total:4 started:4)
2021-07-15 21:18:27.172  INFO 16767 --- [           main] o.a.c.impl.engine.AbstractCamelContext   :     Started route1 (direct://parallelIncidentIngestion)
2021-07-15 21:18:27.172  INFO 16767 --- [           main] o.a.c.impl.engine.AbstractCamelContext   :     Started route2 (direct://getIncidentsOfType)
2021-07-15 21:18:27.172  INFO 16767 --- [           main] o.a.c.impl.engine.AbstractCamelContext   :     Started route3 (direct://getAllIncidents)
2021-07-15 21:18:27.172  INFO 16767 --- [           main] o.a.c.impl.engine.AbstractCamelContext   :     Started route4 (rest://get:incidents:)
2021-07-15 21:18:27.172  INFO 16767 --- [           main] o.a.c.impl.engine.AbstractCamelContext   : Apache Camel 3.10.0 (MyCamel) started in 482ms (build:32ms init:350ms start:100ms)
2021-07-15 21:18:27.177  INFO 16767 --- [           main] c.c.demos.MySpringBootApplication        : Started MySpringBootApplication in 2.559 seconds (JVM running for 2.826) <1>
----
<1> This line indicates the application is ready to serve requests

== Architectural Approach
My initial investigation into this problem domain was skewed by the time constraint of two hours. I set myself up for some bad ideas in the very beginning. Generally, I try to do contract by design, and wanted to ensure my JSON output would be sane before moving on to parsing, assuming the source APIs were sane. By the time I had created my initial model and tested its output over JSON, I was already 1.5 hours from creating the git repository. Again feeling the time pressure, I moved on to creating the parallell calls to the source endpoints. These seemed to take a while to complete their responses. After fussing for a while over why that might be, I decided the requirement to return a response in under two seconds must have been after receiving the source data. In Camel, I could issue a timeout and "send what I have" if I wanted to, and figured that I could discuss this during review. I didn't want to bottleneck any more time, so I moved on to examining the output. By that time, though, it was pencils down and time to submit.

In my second draft, I wanted to see how quickly I could get to a place I was happy with, that would let me talk about the code I wrote versus the discovery I did. I'm still not nearly satisfied with what I wrote, especially considering the problem domain. If I had to do this again, I would have thoroughly explored the API with `curl` and `jq` to see if there were patterns that I could take advantaged of. Even now, I know my results are probably off, and I would take some time with `jq` to count and verify manually.

== Next Steps

Depending on the use case I'd add some caching patterns into this instead of gathering all the data at once. Populating a cache by employee and doing the work serially would likely be faster and less expensive.

I'd add unit testing, e2e, dockerization, deployment pipeline for GitLab. I'd also add prometheus metrics exposure, and SSO authentication.
