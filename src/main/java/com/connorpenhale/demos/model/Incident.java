package com.connorpenhale.demos.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Incident implements Serializable {

    private String type;
    private String priority;
    private String machineIp;
    private float timestamp;

}
