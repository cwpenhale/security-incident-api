package com.connorpenhale.demos.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IncidentResponse {

    private List<Incident> incidents = new ArrayList<>();

    @JsonProperty("count")
    public Integer getCount(){
        return incidents == null ? 0 : getIncidents().size();
    };
}
