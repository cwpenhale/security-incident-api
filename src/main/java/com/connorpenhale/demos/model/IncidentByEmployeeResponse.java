package com.connorpenhale.demos.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class IncidentByEmployeeResponse {

    private IncidentResponse low = new IncidentResponse();
    private IncidentResponse medium = new IncidentResponse();
    private IncidentResponse high = new IncidentResponse();
    private IncidentResponse critical = new IncidentResponse();

}
