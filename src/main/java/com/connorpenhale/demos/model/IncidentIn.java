package com.connorpenhale.demos.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class IncidentIn {

    @JsonProperty("reported_by")
    @JsonAlias("employee_id")
    private Long reportedBy;
    private String priority;
    @JsonProperty("source_ip")
    @JsonAlias({"machine_ip", "ip", "internal_ip", "identifier"})
    private String sourceIp;
    private float timestamp;

}
