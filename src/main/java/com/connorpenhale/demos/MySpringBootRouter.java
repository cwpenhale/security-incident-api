package com.connorpenhale.demos;

import com.connorpenhale.demos.model.Incident;
import com.connorpenhale.demos.model.IncidentByEmployeeResponse;
import com.connorpenhale.demos.model.IncidentIn;
import com.connorpenhale.demos.model.IncidentResponse;
import org.apache.camel.AggregationStrategy;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.jackson.ListJacksonDataFormat;
import org.apache.camel.http.common.HttpMethods;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.processor.aggregate.GroupedMessageAggregationStrategy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A simple Camel route that triggers from a timer and calls a bean and prints to system out.
 * <p/>
 * Use <tt>@Component</tt> to make Camel auto detect this route when starting.
 */
@Component
public class MySpringBootRouter extends RouteBuilder {

    List<String> endpoints = new ArrayList() {{
        add("denial");
        add("intrusion");
        add("executable");
        add("misuse");
        add("unauthorized");
        add("probing");
        add("other");
    }};

    @Override
    public void configure() {
        restConfiguration().component("netty-http").host("localhost").port(9000).bindingMode(RestBindingMode.off);

        JacksonDataFormat incidentDF = new ListJacksonDataFormat(IncidentIn.class);

        from("direct:parallelIncidentIngestion")
            .split().constant(endpoints)
                .aggregationStrategy(new GroupedMessageAggregationStrategy())
                .shareUnitOfWork()
                .parallelProcessing()
                    .setHeader("type", body())
                    .to("direct:getIncidentsOfType")
                .end();

        from("direct:getIncidentsOfType")
            .setBody().constant(null)
            .setHeader("Authorization").constant("Basic ZWxldmF0ZWludGVydmlld3M6RWxldmF0ZVNlY3VyaXR5SW50ZXJ2aWV3czIwMjE=")
            .setHeader(Exchange.HTTP_METHOD).constant(HttpMethods.GET)
            .setHeader(Exchange.HTTP_PATH, constant(null))
            .setHeader(Exchange.HTTP_URI, simple("https://incident-api.use1stag.elevatesecurity.io/incidents/${headers.type}/"))
            .to("https://incident-api.use1stag.elevatesecurity.io/")
            .setBody().jsonpath("$.results")
            .marshal().json(JsonLibrary.Jackson)
            .unmarshal(incidentDF);

        from("direct:getAllIncidents")
            .log("Getting all incidents...")
            .to("direct:parallelIncidentIngestion")
            .log("Got all incidents, processing...")
            .split().body().aggregationStrategy(new AggregationStrategy() {
                    @Override
                    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
                        Map<Long, IncidentByEmployeeResponse> oldResponse;
                        oldResponse = oldExchange == null ? new HashMap<>() : oldExchange.getIn().getBody(Map.class);
                        Map<Long, IncidentByEmployeeResponse> newResponse = newExchange.getIn().getBody(Map.class);
                        if(newResponse == null){
                            newExchange.getIn().setBody(oldResponse);
                            return newExchange;
                        }
                        newResponse.forEach((k,v) -> {
//                            log.info(String.format("new resp: %s key --  %s c %s h %s m %s l",
//                                    k,
//                                    v.getCritical().getIncidents().size(),
//                                    v.getHigh().getIncidents().size(),
//                                    v.getMedium().getIncidents().size(),
//                                    v.getLow().getIncidents().size()));
                            if(oldResponse.containsKey(k)) {
                                IncidentByEmployeeResponse iber = oldResponse.get(k);
                                List<Incident> critical = iber.getCritical() == null ? new ArrayList<>() : iber.getCritical().getIncidents();
                                critical.addAll(v.getCritical().getIncidents());
                                iber.setCritical(new IncidentResponse(critical));
                                List<Incident> high = iber.getHigh() == null ? new ArrayList<>() : iber.getHigh().getIncidents();
                                high.addAll(v.getHigh().getIncidents());
                                iber.setHigh(new IncidentResponse(high));
                                List<Incident> medium = iber.getMedium() == null ? new ArrayList<>() : iber.getMedium().getIncidents();
                                medium.addAll(v.getMedium().getIncidents());
                                iber.setMedium(new IncidentResponse(medium));
                                List<Incident> low = iber.getLow() == null ? new ArrayList<>() : iber.getLow().getIncidents();
                                low.addAll(v.getLow().getIncidents());
                                iber.setLow(new IncidentResponse(low));
                                oldResponse.put(k, iber);
                            }else{
                                oldResponse.put(k,v);
                            }
                        });
                        newExchange.getIn().setBody(oldResponse);
                        return newExchange;
                    }
                })
                .process(e -> {
                    String type = e.getIn().getHeader("type", String.class);
                    List<IncidentIn> incidentsIn = (List<IncidentIn>) e.getIn().getBody(List.class);
                    Map<Long, IncidentByEmployeeResponse> response = new HashMap<>();
                    Map<Long, List<IncidentIn>> incidentsByEmployee =  incidentsIn.stream()
                            .filter(i -> i.getReportedBy() != null)
                            .collect(Collectors.groupingBy(IncidentIn::getReportedBy));
//                    incidentsByEmployee.forEach((k,v)->{
//                        log.info(String.format("type %s - k %s #%s", type, k, v.size()));
//                    });
                    incidentsByEmployee.forEach((employee,in) -> {
                        Map<String, List<Incident>> incidientsByPriority = in.stream().map(incidentIn ->
                                        new Incident(type,
                                                incidentIn.getPriority(),
                                                incidentIn.getSourceIp(),
                                                incidentIn.getTimestamp()))
                                .collect(Collectors.groupingBy(Incident::getPriority, Collectors.toList()));
                        IncidentByEmployeeResponse oneT = new IncidentByEmployeeResponse();
                        oneT.setCritical(new IncidentResponse(incidientsByPriority.containsKey("critical") ? incidientsByPriority.get("critical") : new ArrayList<>()));
                        oneT.setHigh(new IncidentResponse(incidientsByPriority.containsKey("high") ? incidientsByPriority.get("high") : new ArrayList<>()));
                        oneT.setMedium(new IncidentResponse(incidientsByPriority.containsKey("medium") ? incidientsByPriority.get("medium") : new ArrayList<>()));
                        oneT.setLow(new IncidentResponse(incidientsByPriority.containsKey("low") ? incidientsByPriority.get("low") : new ArrayList<>()));
                        response.put(employee, oneT);
                    });
                    e.getIn().setBody(response);
                })
            .end()
            .log("done processing")
            .marshal().json(JsonLibrary.Jackson);

        rest("incidents")
            .get("")
            .to("direct:getAllIncidents");
    }

}
